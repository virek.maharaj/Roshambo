import java.util.Scanner;

/**
 * Created by virek.maharaj on 2017/05/15.
 */
public class PlayerClass
{
    static char select;
    static RoshamboClass.Move user_move;

    public static RoshamboClass.Move getUserMove()
    {
        /*Scanner Object*/
        Scanner choice = new Scanner(System.in);
        System.out.print("Rock, paper,scissors,lizard or spock?(R/P/S/L/O) : ");
        String selection = choice.next();

        //set Rock for r|R ,Paper for p|P, Scissors for s|S
        if(selection.charAt(0)=='r' || selection.charAt(0) == 'R')
        {
            select='R';
            return user_move= RoshamboClass.Move.ROCK;
        }
        else if(selection.charAt(0)=='p' || selection.charAt(0) == 'P')
        {
            select='P';
            return user_move= RoshamboClass.Move.PAPER;
        }
        else if(selection.charAt(0)=='s' || selection.charAt(0) == 'S')
        {
            select='S';
            return user_move= RoshamboClass.Move.SCISSORS;
        }
        else if(selection.charAt(0)=='l' || selection.charAt(0) == 'L')
        {
            select='L';
            return user_move= RoshamboClass.Move.LIZARD;
        }
        else if(selection.charAt(0)=='o' || selection.charAt(0) == 'O')
        {
            select='O';
            return user_move= RoshamboClass.Move.SPOCK;
        }
        else
        {
            select='D';
            return user_move= RoshamboClass.Move.ERROR;
        }
    }
}
