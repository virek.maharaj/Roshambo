/**
 * Created by virek.maharaj on 2017/05/05.
 */
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.Random;

public class RoshamboClass {
    //create enum for the option moves
    public enum Move{ROCK,PAPER,SCISSORS,LIZARD,SPOCK,ERROR}
    public static void main(String[] args) {
        System.out.println("Welcome to the game of Roshambo\n");
        /*Get user choice*/
        boolean playselection = true;
        while (playselection == true)
        {
            PlayerClass.user_move = PlayerClass.getUserMove();
            System.out.println("You choose:\n" + PlayerClass.user_move);
            PCClass.pc_move = PCClass.getPCMove();
            System.out.println("PC choose:\n" + PCClass.pc_move);
            //System.out.println("You selected : " + selection);

        /*Check if the game is a tie*/
            if (PCClass.pc_move == PlayerClass.user_move) {
                System.out.println("Tie\n");
            }
        /*Check who wins*/
            else {
                switch (PlayerClass.select) {
                    case 'R':
                        if (PCClass.pc_move == Move.PAPER || PCClass.pc_move == Move.SPOCK) {
                            System.out.println("You Lose\n");
                        } else {
                            System.out.println("You Win\n");
                        }
                        break;
                    case 'P':
                        if (PCClass.pc_move == Move.SCISSORS || PCClass.pc_move == Move.LIZARD) {
                            System.out.println("You Lose\n");
                        } else {
                            System.out.println("You Win\n");
                        }
                        break;
                    case 'S':
                        if (PCClass.pc_move == Move.ROCK || PCClass.pc_move == Move.LIZARD) {
                            System.out.println("You Lose\n");
                        } else {
                            System.out.println("You Win\n");
                        }
                        break;
                    case 'L':
                        if (PCClass.pc_move == Move.ROCK || PCClass.pc_move == Move.SCISSORS) {
                            System.out.println("You Lose\n");
                        } else {
                            System.out.println("You Win\n");
                        }
                        break;
                    case 'O':
                        if (PCClass.pc_move == Move.LIZARD || PCClass.pc_move == Move.PAPER) {
                            System.out.println("You Lose\n");
                        } else {
                            System.out.println("You Win\n");
                        }
                        break;
                    default:
                        System.out.println("Error\n");
                        break;

                }
            }
            try
            {
                System.out.println("Would you like to play again?True or false");
                Scanner playchoice = new Scanner(System.in);
                playselection = playchoice.nextBoolean();
                if (playselection == true) {
                    System.out.println("Goodluck\n");
                } else {
                    System.out.println("Bye\n");
                }
            } catch (InputMismatchException e)
            {
                System.out.println("Invalid input!");

            }

        }
    }
}
