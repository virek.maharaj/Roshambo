import java.util.Random;

/**
 * Created by virek.maharaj on 2017/05/15.
 */
public class PCClass
{
    static RoshamboClass.Move pc_move;
    public static RoshamboClass.Move getPCMove()
    {
        /*Random number generator*/
        Random random_option = new Random();
        /*Get the PC choice first*/
        int pc_choice = random_option.nextInt(5);

        if(pc_choice==0)
        {
            return pc_move= RoshamboClass.Move.ROCK;
        }
        else if(pc_choice==1)
        {
            return pc_move= RoshamboClass.Move.PAPER;
        }
        else if(pc_choice==2)
        {
            return pc_move= RoshamboClass.Move.SCISSORS;
        }
        else if(pc_choice==3)
        {
            return pc_move=RoshamboClass.Move.LIZARD;

        }
        else
        {
            return pc_move=RoshamboClass.Move.SPOCK;
        }

    }
}
